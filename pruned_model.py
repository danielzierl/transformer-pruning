from torch.utils.data import DataLoader, Dataset
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import utils
import random

# ------------------------------
lr = 0.02
num_of_epochs = 5
batch_size = 200
hidden_size = 100

num_of_classes = 4
input_size = 2
# ------------------------------
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# device = torch.device('cpu')
if not torch.cuda.is_available():
    print("Beware!! using cpu")


class Classification2DDataset(Dataset):
    def __init__(self, transform=None) -> None:
        self.dataX = np.loadtxt("./data/data.txt")
        k_means = KMeans(n_clusters=4, max_iter=300, n_init="auto")

        self.labels = k_means.fit_predict(self.dataX)
        self.transform = transform

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        item = (self.dataX[index], self.labels[index])
        if self.transform:
            item = self.transform(item)
        return item


class toTensor:
    def __call__(self, tuple) -> any:
        return (
            torch.from_numpy(np.asarray(tuple[0])),
            torch.from_numpy(np.asarray(tuple[1])),
        )


# ------------------------------

dataset = Classification2DDataset(transform=toTensor())
dataloader = DataLoader(
    dataset=dataset, batch_size=batch_size, shuffle=True, num_workers=2
)
loss_fcn = nn.CrossEntropyLoss()

# ------------------------------


class NeuralNet(torch.nn.Module):
    def __init__(self) -> None:
        super(NeuralNet, self).__init__()
        self.l1 = nn.Linear(input_size, hidden_size)
        self.l2 = nn.Linear(hidden_size, num_of_classes)
        self.input_size = input_size

    def forward(self, x):
        x = x.view(-1, self.input_size)
        out = self.l1(x)
        a1 = out.detach()
        out = nn.functional.relu(out)
        out = self.l2(out)
        a2 = out.detach()
        # activations = torch.cat(
        #     (a1.view(x.shape[0], -1), a2.view(x.shape[0], -1)), dim=1
        # )
        return out, a1.view(x.shape[0], -1)

    def train_until_acc(self, dataloader):
        optimizer = torch.optim.SGD(self.parameters(), lr=lr)
        for epoch in range(num_of_epochs):
            for i, (dataX, labels) in enumerate(dataloader):
                labels = labels.type(torch.LongTensor)
                labels = labels.to(device)
                dataX = dataX.to(device)
                dataX = dataX.to(torch.float32)
                output, _ = self(dataX)
                loss = loss_fcn(output, labels)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                if i % 100 == 0:
                    acc = evaluateAccuracy(self,dataloader)
                    print(f"epoch={epoch}, loss={loss}, accuracy={acc}")
                    if acc > 0.99:
                        return


    def init(self):
        print(sum(p.numel() for p in self.parameters()), "parameters")
        self.train_until_acc(dataloader=dataloader)
        utils.plot_result_of_classifying(model=self, data=dataset.dataX)

    def get_batch(self, split, batch_size):
        dataset = Classification2DDataset(transform=toTensor())
        dataloader_cust = DataLoader(
            dataset=dataset, batch_size=batch_size, shuffle=True, num_workers=2
        )
        # full_data = [(dataX,label) for (dataX,label) in dataloader_cust]
        # rand_n = random.randint(0, len(full_data)-1)
        # dataX,labels = full_data[rand_n]
        for dataX, labels in dataloader_cust:
            dataX = dataX.to(device)
            dataX = dataX.to(torch.float32)
            self.to(device)
            labels = labels.to(device)
            output, act = self(dataX)
            _, y = torch.max(output, dim=1)
            # yield torch.cat((act, dataX), dim=1), y
            # print(dataX)
            yield act, y, (dataX[0][0], dataX[0][1])

def evaluateAccuracy(model, dataloader):
    corr = 0
    for i, (dataX, labels) in enumerate(dataloader):
        labels = labels.type(torch.LongTensor)
        dataX = dataX.to(device)
        labels = labels.to(device)
        dataX = dataX.to(torch.float32)
        output, act = model(dataX)
        _, predicted = torch.max(output, dim=1)
        corr += (predicted).eq(labels).sum()
    acc = corr / (len(dataset))
    return acc.item()