import torch
import matplotlib.pyplot as plt
import numpy as np


def plot_result_of_classifying(
    model,
    data=None,
    name="unanmed_plot",
    grid_size=100,
    basic_marker_size=1000,
):
    device = torch.device("cpu")
    model.to(device)
    X = torch.linspace(-15, 15, grid_size).to(device)
    Y = torch.linspace(-15, 15, grid_size).to(device)

    fig = plt.figure()
    result_groups = {}
    plt.xlabel("x")
    plt.ylabel("y")
    for x in X:
        for y in Y:
            res, _ = model(torch.tensor([x, y], device=device))
            _, max_index = torch.max(res, dim=1)
            # print(max_index)
            max_index = max_index.item()
            if not max_index in result_groups:
                result_groups[max_index] = torch.tensor([x, y], device=device).view(
                    1, 2
                )
            else:
                result_groups[max_index] = torch.cat(
                    [
                        result_groups[max_index],
                        torch.tensor([x, y], device=device).view(1, 2),
                    ]
                )
    for group_key in result_groups:
        plt.scatter(
            result_groups[group_key][:, 0].to(torch.device("cpu")),
            result_groups[group_key][:, 1].to(torch.device("cpu")),
            s=basic_marker_size / grid_size,
            marker=",",
        )
    if not data is None:
        plt.scatter(data[:, 0], data[:, 1])
    # plt.show()

    plt.savefig("out.png")


def plot_transformer(
    transf,
    gen,
    grid_size=10,
    basic_marker_size=1000,
):
    device = torch.device("cpu")
    # transf.to(device)
    X = torch.linspace(-15, 15, grid_size).to(device)
    Y = torch.linspace(-15, 15, grid_size).to(device)

    fig = plt.figure()
    result_groups = {}
    plt.xlabel("x")
    plt.ylabel("y")
    for x in X:
        for y in Y:
            xx, yy, loc = next(gen)
            res, _ = transf(xx, yy)
            x, y = loc
            _, max_index = torch.max(res, dim=1)
            max_index = max_index.item()
            if not max_index in result_groups:
                result_groups[max_index] = torch.tensor([x, y], device=device).view(
                    1, 2
                )
            else:
                result_groups[max_index] = torch.cat(
                    [
                        result_groups[max_index],
                        torch.tensor([x, y], device=device).view(1, 2),
                    ]
                )
    for group_key in result_groups:
        plt.scatter(
            result_groups[group_key][:, 0].to(torch.device("cpu")),
            result_groups[group_key][:, 1].to(torch.device("cpu")),
            s=basic_marker_size / grid_size,
            marker=",",
        )

    plt.savefig("out_tr.png")


def plot_tensor(ten: torch.Tensor, label: str = "", x=0, y=0, pos=0):
    set_subplot(x, y, pos)
    ten_num: np.ndarray = ten.detach().numpy()
    # print(np.percentile(ten_num,0.01))
    ten_num[ten_num < np.percentile(ten_num, 0.999)] = 0
    plt.imshow(ten_num, cmap="jet")
    plt.title(label=label)


def set_subplot(x, y, pos):
    plt.subplot(x, y, pos)
    plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0.7)


def new_plot():
    plt.figure(dpi=1500)


def save_plotting_tensor(outfile):
    plt.savefig(outfile)


def measure_module_sparsity(module, weight=True, bias=False, use_mask=False):
    num_zeros = 0
    num_elements = 0

    if use_mask == True:
        for buffer_name, buffer in module.named_buffers():
            if "weight_mask" in buffer_name and weight == True:
                num_zeros += torch.sum(buffer == 0).item()
                num_elements += buffer.nelement()
            if "bias_mask" in buffer_name and bias == True:
                num_zeros += torch.sum(buffer == 0).item()
                num_elements += buffer.nelement()
    else:
        for param_name, param in module.named_parameters():
            if "weight" in param_name and weight == True:
                num_zeros += torch.sum(param == 0).item()
                num_elements += param.nelement()
            if "bias" in param_name and bias == True:
                num_zeros += torch.sum(param == 0).item()
                num_elements += param.nelement()

    try:
        sparsity = num_zeros / num_elements
    except:
        sparsity = "error value"

    return num_zeros, num_elements, sparsity


def measure_global_sparsity(model, weight=True, bias=False, linear_use_mask=False):
    num_zeros = 0
    num_elements = 0

    for module_name, module in model.named_modules():
        if isinstance(module, torch.nn.Linear):
            module_num_zeros, module_num_elements, _ = measure_module_sparsity(
                module, weight=weight, bias=bias, use_mask=linear_use_mask
            )
            num_zeros += module_num_zeros
            num_elements += module_num_elements
    try:
        sparsity = num_zeros / num_elements
    except:
        sparsity = "error value"

    return num_zeros, num_elements, sparsity
