import logging
from utils import measure_global_sparsity
import torch_pruning as tp
import torch
import copy


def do_pruning(
    layer: str,
    model_var,
    sensitivity,
    acc_before,
    DG: tp.DependencyGraph,
    dataloader,
    train_until_acc,
    evaluateAccuracy,
    indx,
):
    print(
        measure_global_sparsity(model_var, linear_use_mask=False),
        evaluateAccuracy(model_var, dataloader),
    )
    copy_model = copy.deepcopy(model_var)
    layer_obj = getattr(model_var, layer)

    DG.build_dependency(model_var, example_inputs=torch.rand(2, dtype=torch.float32))

    pruning_group = DG.get_pruning_group(
        layer_obj, tp.prune_linear_out_channels, idxs=indx
    )
    pruning_group.exec()

    model_var = train_until_acc(model_var, dataloader)
    acc = evaluateAccuracy(model_var, dataloader)
    print(f" acc_before={acc_before}, acc={acc}")
    if sensitivity < abs(acc - acc_before):
        model_var = copy_model

    print(
        measure_global_sparsity(model_var, linear_use_mask=False),
        evaluateAccuracy(model_var, dataloader),
    )
    return model_var
