import pruned_model
import transformer_model
import torch
from torch.optim.lr_scheduler import CosineAnnealingLR
import utils
import logging
# -----------------------------
if torch.cuda.is_available():
    print("running on gpu")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# device = torch.device('cpu')
max_iters = 1000
eval_interval = 100
learning_rate_start = 2e-3
learning_rate_end = 1e-4
batch_size = 100
# -----------------------------
logging.basicConfig(level=logging.INFO)

def train_transformer_on_another_model(
    prunner: transformer_model.FullTransformer, pruned: pruned_model.NeuralNet
):
    optimizer = torch.optim.AdamW(prunner.parameters(), lr=learning_rate_start)
    scheduler = CosineAnnealingLR(
        optimizer,
        T_max=max_iters,  # Maximum number of iterations.
        eta_min=learning_rate_end,  # Minimum learning rate.
    )
    for iter in range(max_iters):
        # every once in a while evaluate the loss on train and val sets
        gen = pruned.get_batch("train", batch_size)
        if iter % eval_interval == 0 or iter == max_iters - 1:
            logging.debug("calling estimate")
            losses = prunner.estimate_loss(gen, make_pictures=True)
            print(
                f"step {iter}: train loss {losses['train']:.4f}, val loss {losses['val']:.4f}, lr {scheduler.get_last_lr()}"
            )

        # sample a batch of data
        xb, yb, _ = next(gen)

        # evaluate the loss
        logits, loss = prunner(xb, yb)
        optimizer.zero_grad(set_to_none=True)
        loss.backward()
        optimizer.step()
        scheduler.step()


if __name__ == "__main__":
    prunner = transformer_model.FullTransformer(block_size=100)
    pruned = pruned_model.NeuralNet()
    prunner, pruned = prunner.to(device), pruned.to(device)
    
    pruned.init()
    train_transformer_on_another_model(pruned=pruned, prunner=prunner)
    utils.plot_transformer(transf=prunner, gen=pruned.get_batch("train", batch_size=1))
