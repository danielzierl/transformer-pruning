import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.optim.lr_scheduler import CosineAnnealingLR, StepLR
import utils
import logging
import math

# hyperparameters
batch_size = 32
learning_rate_start = 2e-3
learning_rate_end = 1e-4
device = "cuda" if torch.cuda.is_available() else "cpu"
# device = torch.device('cpu')
print(device)
eval_iters = 16
n_embd = 8
n_head = 1
n_layer = 1
dropout = 0.0
vocab_size = 500
out_size = 4
# ------------
estim_iteration = 0
col_size = 6


class Head(nn.Module):
    """one head of self-attention"""

    picnum: int = 1

    def __init__(self, head_size, block_size):
        super().__init__()
        self.key = nn.Linear(n_embd, head_size, bias=False)
        self.query = nn.Linear(n_embd, head_size, bias=False)
        self.value = nn.Linear(n_embd, head_size, bias=False)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x, targets):
        B, T, C = x.shape
        k = self.key(x)  # (B,T,C)
        q = self.query(x)  # (B,T,C)
        wei: torch.Tensor = (
            q @ k.transpose(-2, -1) * C**-0.5
        )  # (B, T, C) @ (B, C, T) -> (B, T, T)
        wei = F.softmax(wei, dim=-1)  # (B, T, T)
        wei = self.dropout(wei)
        v = self.value(x)  # (B,T,C)
        out = wei @ v  # (B, T, T) @ (B, T, C) -> (B, T, C)
        if targets is not None:
            utils.plot_tensor(
                ten=wei[0].cpu(),
                label=targets[0].item(),
                x=math.ceil(eval_iters * 3 / col_size),
                y=col_size,
                pos=((self.picnum - 1) % (eval_iters * 3) + 1),
            )
            self.picnum += 1
            utils.plot_tensor(
                ten=wei.mean(dim=(0)).cpu(),
                x=math.ceil(eval_iters * 3 / col_size),
                y=col_size,
                pos=((self.picnum - 1) % (eval_iters * 3) + 1),
            )

            self.picnum += 1
            utils.plot_tensor(
                ten=wei.mean(dim=(0, 1)).view(1, -1).repeat(50, 1).cpu(),
                x=math.ceil(eval_iters * 3 / col_size),
                y=col_size,
                pos=((self.picnum - 1) % (eval_iters * 3) + 1),
            )
            print(
                wei.mean(dim=(0, 1)).view(1, -1).add(-1/50).multiply(50),
            )
            self.picnum += 1
        return out


class MultiHeadAttention(nn.Module):
    """multiple heads of self-attention in parallel"""

    def __init__(self, num_heads, head_size, block_size):
        super().__init__()
        self.heads = nn.ModuleList(
            [Head(head_size, block_size) for _ in range(num_heads)]
        )
        self.proj = nn.Linear(n_embd, n_embd)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x, targets):
        out = torch.cat([head(x, targets) for head in self.heads], dim=-1)
        out = self.dropout(self.proj(out))
        return out


class FeedFoward(nn.Module):
    """a simple linear layer followed by a non-linearity"""

    def __init__(self, n_embd):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(n_embd, 4 * n_embd),
            nn.ReLU(),
            nn.Linear(4 * n_embd, n_embd),
            nn.Dropout(dropout),
        )

    def forward(self, x):
        return self.net(x)


class Block(nn.Module):
    """Transformer block: communication followed by computation"""

    def __init__(self, n_embd, n_head, block_size):
        # n_embd: embedding dimension, n_head: the number of heads we'd like
        super().__init__()
        head_size = n_embd // n_head
        self.sa = MultiHeadAttention(n_head, head_size, block_size)
        self.ffwd = FeedFoward(n_embd)
        self.ln1 = nn.LayerNorm(n_embd)
        self.ln2 = nn.LayerNorm(n_embd)

    def forward(self, x):
        x, targets = x
        x = x + self.sa(self.ln1(x), targets)
        x = x + self.ffwd(self.ln2(x))
        return x


class FullTransformer(nn.Module):
    def __init__(self, block_size):
        super().__init__()
        # each token directly reads off the logits for the next token from a lookup table
        self.token_embedding_table = nn.Embedding(vocab_size, n_embd)
        self.block_size = block_size
        self.position_embedding_table = nn.Embedding(block_size, n_embd)
        self.blocks = nn.Sequential(
            *[
                Block(n_embd, n_head=n_head, block_size=block_size)
                for _ in range(n_layer)
            ]
        )
        self.ln_f = nn.LayerNorm(n_embd)  # final layer norm
        self.lm_head = nn.Linear(n_embd, out_size)

    def forward(self, idx, targets=None, make_pictures: bool = False):
        # idx = idx.to(torch.int32)
        idx = vocab_preprocess(idx)
        # targets = vocab_preprocess(targets)
        B, T = idx.shape

        # idx and targets are both (B,T) tensor of integers
        tok_emb = self.token_embedding_table(idx)  # (B,T,C)
        pos_emb = self.position_embedding_table(torch.arange(T, device=device))  # (T,C)
        x = tok_emb + pos_emb  # (B,T,C)
        x = self.blocks((x, targets) if make_pictures else (x, None))  # (B,T,C)
        x = self.ln_f(x)  # (B,T,C)
        logits = self.lm_head(x)  # (B,T,vocab_size)
        logits = logits[:, -1, :]
        if targets is None:
            loss = None
        else:
            B, C = logits.shape
            # logits = logits.view(B, C)
            targets = targets.view(B).to(torch.long)
            loss = F.cross_entropy(logits, targets)

        return logits, loss

    def generate(self, idx, max_new_tokens):
        for _ in range(max_new_tokens):
            idx_cond = idx[:, -self.block_size :]
            logits, loss = self(idx_cond)
            logits = logits[:, -1, :]  # becomes (B, C)
            probs = F.softmax(logits, dim=-1)  # (B, C)
            idx_next = torch.multinomial(probs, num_samples=1)  # (B, 1)
            idx = torch.cat((idx, idx_next), dim=1)  # (B, T+1)
        return idx

    @torch.no_grad()
    def estimate_loss(self, gen, make_pictures=False):
        global estim_iteration
        out = {}
        if make_pictures:
            utils.new_plot()
        self.eval()
        for split in ["train", "val"]:
            losses = torch.zeros(eval_iters)
            for k in range(eval_iters):
                # print(f"xsize {X.shape}, ysize {Y.shape}")
                X, Y, _ = next(gen)
                logits, loss = self(
                    X, Y, make_pictures=make_pictures if split == "val" else False
                )
                # print(logits[0])
                # print(Y[0])
                losses[k] = loss.item()
            out[split] = losses.mean()
        if make_pictures:
            utils.save_plotting_tensor(
                outfile=f"eval_iter_run2_{estim_iteration}.jpg",
            )
            estim_iteration += 1
        self.train()
        return out


def vocab_preprocess(tok_tensor: torch.Tensor):
    # print(tok_tensor)
    return (tok_tensor.multiply(5).add(250)).round().to(torch.int32)
